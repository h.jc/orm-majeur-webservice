/* eslint-disable no-shadow */
import mysql from 'mysql';
import { app } from 'swaggest/dist/src/swaggest';
import {
  QueryFilterOrder, FindByIdOptions, ModelConfig,
} from '../../src/models/model';
import { ConnexionError } from '../../src/utils/connexionError.utils';

require('dotenv').config();

/**
 *
 *
 * @export
 * @interface QueryFilter
 */
export interface QueryFilter {
  where?: Record<string, any>;
  limit?: number;
  page?: number;
  sort?: string;
  order?: QueryFilterOrder;
}

/**
 *
 *
 * @export
 * @class MysqlConnector
 */
export default class MysqlConnector {
  static config: ModelConfig;

  id?: string | number;

  static connect(config: ModelConfig): void {
    this.config = config;
    this.config.connection = mysql.createConnection({
      host: app.config.db_host,
      user: app.config.db_user,
      password: app.config.db_passwd,
      database: app.config.db_database,
    });
    this.config.connection.connect();
  }


  static async get_info(query: string, values: Array<any> = []): Promise<void> {
    return new Promise((resolve, reject) => {
      this.config.connection.query(query, values, async (err: any, results: any) => {
        if (err) { return reject(err); }
        console.log(results);
        const data = results;
        resolve(data);
      });
    });
  }

  /**
   *
   *
   * @static
   * @param {object} dataOrModel
   * @returns {Record<string, any>}
   * @memberof MysqlConnector
   */
  static async create(dataOrModel: object, config: ModelConfig): Promise<Record<string, any>> {
    this.connect(config);
    const dataObject: Array<any> = [];
    const dataKeys: Array<any> = [];
    Object.values(dataOrModel).map((element: any) => {
      dataObject.push(element);
    });
    Object.keys(dataOrModel).map((element: any) => {
      dataKeys.push(element);
    });
    let str = '';
    let dataString = '';
    dataObject.forEach(() => {
      if (str.length !== 0) {
        return str += ', ?';
      }
      return str += '?';
    });
    dataKeys.forEach((element) => {
      if (dataString.length !== 0) {
        return dataString += `, ${element}`;
      }
      return dataString += element;
    });
    let query = `INSERT INTO ${this.config.endpoint}(${dataString}) VALUES(${str})`;
    await this.get_info(query, dataObject);
    query = `SELECT * FROM ${this.config.endpoint} ORDER BY id DESC LIMIT 1`;
    const data: any = await this.get_info(query, []);
    return data[0];
  }


  /**
   *
   *
   * @static
   * @param {QueryFilter} [filter]
   * @returns {Record<string, any>}
   * @memberof MysqlConnector
   */
  static async find(config: ModelConfig, filter?: QueryFilter): Promise<Record<string, any>> {
    this.connect(config);
    let data: any;
    if (filter != null && filter !== undefined) {
      let optionString = '?';
      if (filter.where !== undefined) {
        const whereSplit = filter.where;
        Object.keys(whereSplit).map((key: string) => {
          if (filter.where !== undefined) {
            optionString = `${optionString}${key}=${filter.where[`${key}`]}&`;
          }
        });
      } if (filter.limit !== undefined) {
        optionString = `${optionString} limit=${filter.limit}`;
      } if (filter.page !== undefined) {
        optionString = `${optionString} page=${filter.page}`;
      } if (filter.sort !== undefined) {
        optionString = `${optionString} sort=${filter.sort}`;
      } if (filter.order !== undefined) {
        optionString = `${optionString} order=${filter.order}`;
      }
      const query = `SELECT * FROM ${this.config.endpoint} ${optionString}`;
      data = await this.get_info(query, []);
      return data;
    }
    const query = `SELECT * FROM ${this.config.endpoint} ORDER BY id ASC`;
    data = await this.get_info(query, []);
    this.config.connection.end();
    return data;
  }


  /**
   *
   *
   * @static
   * @param {(string | number)} id
   * @param {FindByIdOptions} [options]
   * @returns {Record<string, any>}
   * @memberof MysqlConnector
   */
  static async findById(id: string | number, config: ModelConfig, options?: FindByIdOptions): Promise<Record<string, any>> {
    this.connect(config);
    let query = `SELECT * FROM ${this.config.endpoint} WHERE id = ${id}`;
    if (options != null && options !== undefined && options.includes.length !== 0) {
      query += options.includes.forEach((element: any) => `FULL JOIN ${element} ON ${this.config.endpoint}.key = ${element}.key`);
    }
    const data: any = await this.get_info(query, []);
    this.config.connection.end();
    return data[0];
  }

  /**
   *
   *
   * @static
   * @param {T} model
   * @returns {Record<string, any>}
   * @memberof MysqlConnector
   */
  static async updateById(model: Record<string, any>, config: ModelConfig): Promise<Record<string, any>> {
    this.connect(config);
    const dataObject: any[] = [];
    Object.values(model).map((element: any) => {
      dataObject.push(element);
    });
    const dataString = dataObject.forEach((element) => {
      let str = '';
      str = `${str},${element}`;
      return str;
    });
    let query = `UPDATE ${this.config.endpoint} SET ${dataString} WHERE id = ${model.id}`;
    await this.get_info(query, []);
    query = `SELECT * FROM ${this.config.endpoint} WHERE id = ${model.id}`;
    const data: any = await this.get_info(query, []);
    this.config.connection.end();
    return data[0];
  }

  /**
   *
   *
   * @static
   * @param {(string | number)} id
   * @returns {boolean}
   * @memberof MysqlConnector
   */
  static async deleteById(id: string | number, config: ModelConfig): Promise<boolean> {
    this.connect(config);
    const query = `DELETE FROM ${this.config.endpoint} WHERE id = ${id}`;
    await this.get_info(query, []);
    this.config.connection.end();
    return true;
  }

  /**
   *
   *
   * @static
   * @param {Record<string, any>} model
   * @returns {Promise<Record<string, any>>}
   * @memberof MysqlConnector
   */
  static async save(model: Record<string, any>, config: ModelConfig): Promise<Record<string, any>> {
    const data = await MysqlConnector.updateById(model, config);
    return data;
  }


  /**
   *
   *
   * @static
   * @param {Partial<Record<string, any>>} data
   * @returns {Promise<T>}
   * @memberof MysqlConnector
   */
  static async update(model: Record<string, any>, data: Partial<Record<string, any>>, config: ModelConfig): Promise<Record<string, any>> {
    this.connect(config);
    const proto = Object.getPrototypeOf(model);
    const dataObject: any[] = [];
    Object.values(data).map((element: any) => {
      dataObject.push(element);
    });
    let str = '';
    const dataString = dataObject.forEach(() => {
      str += '?';
      return str;
    });
    const query = `UPDATE ${proto.construtor.name} SET ${dataString} WHERE id = ${model.id}`;
    const response: any = await this.get_info(query, []);
    this.config.connection.end();
    return response[0];
  }

  /**
   *
   *
   * @returns {Promise<void>}
   * @memberof MysqlConnector
   */
  static async remove(model: Record<string, any>, config: ModelConfig): Promise<void> {
    if (model.id !== undefined) {
      const result = await MysqlConnector.deleteById(model.id, config);
      if (result === false) {
        throw new ConnexionError('Error at delete element');
      }
    }
  }


  /**
   *
   *
   * @returns {string}
   * @memberof MysqlConnector
   */
  toString(): string {
    return JSON.stringify(this);
  }
}
