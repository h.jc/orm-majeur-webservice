/* eslint-disable linebreak-style */
/* eslint-disable array-callback-return */
/* eslint-disable no-case-declarations */
/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from 'axios';
import { app } from 'swaggest/dist/src/swaggest';
import {
  QueryFilter, FindByIdOptions, RelationType, ModelConfig,
} from '../../src/models/model';

require('dotenv').config();

const api = axios.create({
  baseURL: app.config.db_host || 'http://localhost:3000',
});


/**
 *
 *
 * @export
 * @class ApiCconnector
 */
export default class ApiCconnector {
  static config: ModelConfig;

  id?: string | number;


  /**
   *
   *
   * @static
   * @param {object} dataOrModel
   * @returns {Promise<Record<string, any>>}
   * @memberof ApiCconnector
   */
  static async create(dataOrModel: object): Promise<Record<string, any>> {
    const { data } = await api.post(`${this.config.endpoint}`, dataOrModel);
    return data;
  }


  /**
   *
   *
   * @static
   * @param {QueryFilter} [filter]
   * @returns {Promise<Array<Object>>}
   * @memberof ApiCconnector
   */
  static async find(filter?: QueryFilter): Promise<Array<Record<string, any>>> {
    if (filter != null && filter !== undefined) {
      let optionString = '?';
      if (filter.where !== undefined) {
        const whereSplit = filter.where;
        Object.keys(whereSplit).map((key: string) => {
          if (filter.where !== undefined) {
            optionString = `${optionString}${key}=${filter.where[`${key}`]}&`;
          }
        });
      } if (filter.limit !== undefined) {
        optionString = `${optionString}_limit=${filter.limit}&`;
      } if (filter.page !== undefined) {
        optionString = `${optionString}_page=${filter.page}&`;
      } if (filter.sort !== undefined) {
        optionString = `${optionString}_sort=${filter.sort}&`;
      } if (filter.order !== undefined) {
        optionString = `${optionString}_order=${filter.order}&`;
      }
      const { data } = await api.get(`${this.config.endpoint}${optionString}`);
      return data;
    }
    const { data } = await api.get(`${this.config.endpoint}`);
    return data;
  }


  /**
   *
   *
   * @static
   * @param {(string | number)} id
   * @param {FindByIdOptions} [options]
   * @returns {Promise<Object>}
   * @memberof ApiCconnector
   */
  static async findById(id: string | number, options?: FindByIdOptions): Promise<Record<string, any>> {
    const { data } = await api.get<Record<string, any>>(`${this.config.endpoint}${id}`);
    if (options != null && options !== undefined && options.includes.length !== 0) {
      await Promise.all(options.includes.map(async (include) => {
        if (this.config.relation !== undefined) {
          let key = null;
          switch (this.config.relation[include].type) {
            case RelationType.BelongsTo:
              key = this.config.relation[include].foreignKey;
              if (key != null && key in data) {
                const dataOption = await this.config.relation[include].model.findById(data[key]);
                data[include] = dataOption;
              }
              break;
            case RelationType.HasMany:
              key = this.config.relation[include].foreignKey;
              const filter = { where: { [key]: [id] } };
              if (key != null) {
                const OptionData = await this.config.relation[include].model.find(filter);
                data[include] = OptionData;
              }
              break;

            default:
              break;
          }
        }
      }));
    }
    return data;
  }


  /**
   *
   *
   * @static
   * @param {Object} model
   * @returns {Promise<Object>}
   * @memberof ApiCconnector
   */
  static async updateById(model: Record<string, any>): Promise<Record<string, any>> {
    await api.put(`${this.config.endpoint}${model.id}`, model)
      .catch((err) => {
        console.log(err);
      });
    return model;
  }


  /**
   *
   *
   * @static
   * @param {(string | number)} id
   * @returns {Promise<boolean>}
   * @memberof ApiCconnector
   */
  static async deleteById(id: string | number): Promise<boolean> {
    const response = await api.delete(`${this.config.endpoint}${id}`);
    if (response.status === 200) {
      return true;
    }
    return false;
  }

  /**
   *
   *
   * @static
   * @returns {Promise<Object>}
   * @memberof ApiCconnector
   */
  static async save(model: Record<string, any>): Promise<Record<string, any>> {
    const proto = Object.getPrototypeOf(model);
    const response = await api.put(proto.constructor.config.endpoint + model.id, model);
    return response.data;
  }


  /**
   *
   *
   * @static
   * @param {Partial<Record<string, any>>} data
   * @returns {Promise<Object>}
   * @memberof ApiCconnector
   */
  static async update(model: Record<string, any>, data: Partial<Record<string, any>>): Promise<Record<string, any>> {
    const proto = Object.getPrototypeOf(model);
    const response = await api.put(proto.constructor.config.endpoint + model.id, data);
    return response.data;
  }


  /**
   *
   *
   * @returns {Promise<void>}
   * @memberof ApiCconnector
   */
  static async remove(model: Record<string, any>): Promise<void> {
    const proto = Object.getPrototypeOf(model);
    await api.delete(proto.constructor.config.endpoint + model.id);
  }


  /**
   *
   *
   * @returns {string}
   * @memberof ApiCconnector
   */
  toString(): string {
    return JSON.stringify(this);
  }
}
