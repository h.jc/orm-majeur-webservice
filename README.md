# ORM

ORM Open source develop with NodeJS and Typescript.

# Files

 - src : sources files for the ORM to works
	 - models/model.ts : Generics files of model
	 - models/user.ts : Example of how declare a model
	 - utils : Error classes
 - config/db : connector files to API or MYSQL database.

## How it works ?

It can be use with the micro-framwork Swaggest : `npm install -g git+https://ORM_ACCESS:DayeCisozX8gC3Gbx4JC@gitlab.com/swaggest1/swaggest.git#master`

Or just add .env and the package [dotenv](https://www.npmjs.com/package/dotenv)
and modify in config/db:
in **api_connector.ts** this line with your variable name:

    const  api  = axios.create({
	    baseURL: app.config.db_host ||  'http://localhost:3000',
	    });
and in **mysql_connector.ts** this line with your variable name:

    this.config.connection = mysql.createConnection({
	    host: app.config.db_host,
	    user: app.config.db_user,
	    password: app.config.db_passwd,
	    database: app.config.db_database,
	    });
for example change `app.config.db_host` will become `process.env.db_host`.

Then create a class : 

**User.js**

    import {
    Model, ModelConfig,
    } from  './model';

    export  interface  UserSchema {
	    id:  string  |  number;
	    name:  string;
	    username:  string;
	    }
    export  class  User  extends  Model  implements  UserSchema {
	    id!:  string  |  number;
	    static config:  ModelConfig  = {
		    endpoint:  'users/',
	    };
	    name!:  string;
	    username!:  string;
	    }
