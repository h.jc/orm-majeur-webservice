export class ConnectTypeKeys {
    static readonly MYSQL = 'mysql';

    static readonly API = 'api';
}

export class ErrorMsgKeys {
    static readonly CONNECTOR_NOT_EXIST = 'This connector has not been configured';
}

export class ClassErrorKeys {
    static readonly CONNEXION_ERROR = 'ConnexionError :';
}
