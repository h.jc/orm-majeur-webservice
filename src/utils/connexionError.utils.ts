import { ClassErrorKeys } from "./const.utils"

export class ConnexionError extends Error {

    constructor(message: string) {
        super(message)

        this.name = ClassErrorKeys.CONNEXION_ERROR;
    }
}