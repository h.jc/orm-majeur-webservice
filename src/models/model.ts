/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-new */
/* eslint-disable array-callback-return */
import { app } from 'swaggest/dist/src/swaggest';
import { NonFunctionKeys } from 'utility-types';
import ApiConnector from '../../config/db/api_connector';
import MysqlConnector from '../../config/db/mysql_connector';
import { ConnectTypeKeys, ErrorMsgKeys } from '../utils/const.utils';
import { ConnexionError } from '../utils/connexionError.utils';

type SchemaOf<T extends object> = Pick<T, NonFunctionKeys<T>>;

/**
 *
 *
 * @export
 * @interface FindByIdOptions
 */
export interface FindByIdOptions {
  includes: string[];
}

export enum QueryFilterOrder {
  Asc = 'asc',
  Desc = 'desc',
}

/**
 *
 *
 * @export
 * @interface QueryFilter
 */
export interface QueryFilter {
  where?: Record<string, any>;
  limit?: number;
  page?: number;
  sort?: string;
  order?: QueryFilterOrder;
}

export enum RelationType {
  BelongsTo = 'belongsTo',
  HasMany = 'hasMany',
}


/**
 *
 *
 * @export
 * @interface Relation
 */
export interface Relation {
  /** Type of the relation: hasMany, belongsTo, ... */
  type: RelationType;

  /** The target Model */
  model: any;
  /**
   * The key containing the relation link
   * - on the target model if hasMany
   * - on the current model if belongsTo
   */
  foreignKey: string;
}


/**
 *
 *
 * @export
 * @interface ModelConfig
 */
export interface ModelConfig {
  endpoint: string;

  connection?: any;

  relation?: Record<string, Relation>;
}


export interface ModelClass<T extends Model> {
  new(data: Record<string, any>): T;
  config: ModelConfig;
}

/**
 *
 *
 * @export
 * @abstract
 * @class Model
 */
export abstract class Model {
  protected static config: ModelConfig;

  id?: string | number;

  constructor(modelData: Record<string, any>) {
    Object.assign(this, modelData);
  }

  /**
    *
    *
    * @static
    * @template T
    * @param {(object | T)} dataOrModel
    * @returns {T}
    * @memberof Model
    */
  static async create<T extends Model>(this: ModelClass<T>, dataOrModel: object | T): Promise<T> {
    let data: Record<string, any> = {};
    switch (app.config.db_dialect) {
      case ConnectTypeKeys.MYSQL:
        data = await MysqlConnector.create(dataOrModel, this.config);
        break;
      case ConnectTypeKeys.API:
        data = await ApiConnector.create(dataOrModel);
        break;

      default:
        throw new ConnexionError(ErrorMsgKeys.CONNECTOR_NOT_EXIST);
    }
    return new this(data);
  }

  /**
    *
    *
    * @static
    * @template T
    * @param {QueryFilter} [filter]
    * @returns {Promise<T[]>}
    * @memberof Model
    */
  static async find<T extends Model>(this: ModelClass<T>, filter?: QueryFilter): Promise<T[]> {
    let data: any;
    switch (app.config.db_dialect) {
      case ConnectTypeKeys.MYSQL:
        data = await MysqlConnector.find(this.config, filter);
        break;
      case ConnectTypeKeys.API:
        await ApiConnector.find(filter).then((response) => { data = response; });
        break;

      default:
        throw new ConnexionError(ErrorMsgKeys.CONNECTOR_NOT_EXIST);
    }
    let result: T[] = [];
    if (data != null) {
      result = data.map(((element: Record<string, any>) => new this(element)));
    }
    return result;
  }

  /**
    *
    *
    * @static
    * @template T
    * @param {(string | number)} id
    * @param {FindByIdOptions} [options]
    * @returns {Promise<T>}
    * @memberof Model
    */
  static async findById<T extends Model>(this: ModelClass<T>, id: string | number, options?: FindByIdOptions): Promise<T> {
    let data: ApiConnector | MysqlConnector | Record<string, any> = [];
    switch (app.config.db_dialect) {
      case ConnectTypeKeys.MYSQL:
        data = await MysqlConnector.findById(id, this.config, options);
        break;
      case ConnectTypeKeys.API:
        await ApiConnector.findById(id, options).then((connection) => {
          data = connection;
        }).catch((error) => console.log(error));
        break;

      default:
        throw new ConnexionError(ErrorMsgKeys.CONNECTOR_NOT_EXIST);
    }
    return new this(data);
  }

  /**
    *
    *
    * @static
    * @template T
    * @param {T} model
    * @returns {T}
    * @memberof Model
    */
  static async updateById<T extends Model>(this: ModelClass<T>, model: T): Promise<T> {
    let data: Record<string, any> = {};
    switch (app.config.db_dialect) {
      case ConnectTypeKeys.MYSQL:
        data = await MysqlConnector.updateById(model, this.config);
        console.log(data);
        break;
      case ConnectTypeKeys.API:
        await ApiConnector.updateById(model).then((response) => { data = response; }).catch((error: any) => console.log(error));
        break;

      default:
        throw new ConnexionError(ErrorMsgKeys.CONNECTOR_NOT_EXIST);
    }
    return new this(data);
  }

  /**
    *
    *
    * @static
    * @param {(string | number)} id
    * @returns {Promise<boolean>}
    * @memberof Model
    */
  static async deleteById(id: string | number): Promise<boolean> {
    let data;

    switch (app.config.db_dialect) {
      case ConnectTypeKeys.MYSQL:
        data = await MysqlConnector.deleteById(id, this.config);
        break;
      case ConnectTypeKeys.API:
        data = ApiConnector.deleteById(id);
        break;

      default:
        throw new ConnexionError(ErrorMsgKeys.CONNECTOR_NOT_EXIST);
    }

    return data;
  }

  /**
   *
   *
   * @template T
   * @returns {Promise<T>}
   * @memberof Model
   */
  async save<T extends Model>(): Promise<T> {
    const proto = Object.getPrototypeOf(this);
    let data: any;
    switch (proto.constructor.name) {
      case 'MysqlConnector':
        data = await MysqlConnector.save(this, proto.constructo.config);
        break;
      case 'ApiConnector':
        data = ApiConnector.updateById(this);
        break;
      default:
        break;
    }
    return data;
  }

  /**
   *
   *
   * @template T
   * @param {Partial<Record<string, any>>} data
   * @returns {Promise<T>}
   * @memberof Model
   */
  async update<T extends Model>(data: Partial<Record<string, any>>): Promise<T> {
    const proto = Object.getPrototypeOf(this);
    let response: any;
    switch (proto.constructor.name) {
      case 'MysqlConnector':
        response = await MysqlConnector.update(this, data, proto.constructor.config);
        break;
      case 'ApiConnector':
        response = await ApiConnector.update(this, data);
        break;
      default:
        break;
    }
    return response;
  }

  /**
   *
   *
   * @returns {Promise<void>}
   * @memberof Model
   */
  async remove(): Promise<void> {
    const proto = Object.getPrototypeOf(this);
    switch (proto.constructor.name) {
      case 'MysqlConnector':
        await MysqlConnector.remove(this, proto.constructo.config);
        break;
      case 'ApiConnector':
        await ApiConnector.remove(this).catch((err: any) => { console.log(err); });
        break;
      default:
        break;
    }
  }

  /**
    *
    *
    * @returns {string}
    * @memberof Model
    */
  toString(): string {
    return JSON.stringify(this);
  }
}

/**
         try {
          throw new ConnexionError();
        } catch (e){
            if(e instanceof ConnexionError){
                console.log('out of range');
            }
        }
 */
